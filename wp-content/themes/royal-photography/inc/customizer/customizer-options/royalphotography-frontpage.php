<?php
function royalphotography_blog_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	$wp_customize->add_panel(
		'royalphotography_frontpage_sections', array(
			'priority' => 32,
			'title' => esc_html__( 'Frontpage Sections', 'royal-photography' ),
		)
	);
	


	/*=========================================
	Slider Section
	=========================================*/
	$wp_customize->add_section(
		'slider_setting', array(
			'title' => esc_html__( 'Slider Section', 'royal-photography' ),
			'description'=> __('<a>Note :</a> Image Size Should Be 511*767','royal-photography'),
			'priority' => 1,
			'panel' => 'royalphotography_frontpage_sections',
		)
	);



	$wp_customize->add_setting('royalphotography_slider_tabs', array(
	   'sanitize_callback' => 'wp_kses_post',
	));

	$wp_customize->add_control(new royalphotography_Tab_Control($wp_customize, 'royalphotography_slider_tabs', array(
	   'section' => 'slider_setting',
	   'priority' => 2,
	   'buttons' => array(
	      array(
         	'name' => esc_html__('General', 'royal-photography'),
            'icon' => 'dashicons dashicons-welcome-write-blog',
            'fields' => array(
            	'slider1',
            	'slider2',
            	'slider3',
            	'slider4',
            	'slider5',
            	'slider6'
            ),
            'active' => true,
         ), 
	      array(
            'name' => esc_html__('Style', 'royal-photography'),
        	'icon' => 'dashicons dashicons-art',
            'fields' => array(
                'slider_titlecolor',
                'slider_descriptioncolor',
                'slider_btntxt1color',
				'slider_btnbgcolor'

            ),
     	)
	    
    	),
	))); 


	

	// General Tab

	// Slider 1
	$wp_customize->add_setting( 
    	'slider1',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'slider1',
		array(
		    'label'   		=> __('Slider 1','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);		



	// Slider 2
	$wp_customize->add_setting(
    	'slider2',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'slider2',
		array(
		    'label'   		=> __('Slider 2','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);	


	// Slider 3
	$wp_customize->add_setting(
    	'slider3',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 5,
		)
	);	

	$wp_customize->add_control( 
		'slider3',
		array(
		    'label'   		=> __('Slider 3','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);	


	// Slider 4
	$wp_customize->add_setting(
    	'slider4',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 6,
		)
	);	

	$wp_customize->add_control( 
		'slider4',
		array(
		    'label'   		=> __('Slider 4','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);



	// Slider 5
	$wp_customize->add_setting(
    	'slider5',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 7,
		)
	);	

	$wp_customize->add_control( 
		'slider5',
		array(
		    'label'   		=> __('Slider 5','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);

	// Slider 6
	$wp_customize->add_setting(
    	'slider6',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 7,
		)
	);	

	$wp_customize->add_control( 
		'slider6',
		array(
		    'label'   		=> __('Slider 6','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);




	// Style setting

	// slider title Color
	$slidertitlecolor = esc_html__('#fff', 'royal-photography' );
	$wp_customize->add_setting(
    	'slider_titlecolor',
    	array(
			'default' => $slidertitlecolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'slider_titlecolor',
		array(
		    'label'   		=> __('Title Color','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);


	// slider description Color
	$sliderdescriptioncolor = esc_html__('#fff', 'royal-photography' );
	$wp_customize->add_setting(
    	'slider_descriptioncolor',
    	array(
			'default' => $sliderdescriptioncolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'slider_descriptioncolor',
		array(
		    'label'   		=> __('Description Color','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// slider btntxt1 Color
	$sliderbtntxt1color = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'slider_btntxt1color',
    	array(
			'default' => $sliderbtntxt1color,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'slider_btntxt1color',
		array(
		    'label'   		=> __('Button Text Color','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// slider btnbg Color
	$sliderbtnbgcolor = esc_html__('#FFAF33', 'royal-photography' );
	$wp_customize->add_setting(
    	'slider_btnbgcolor',
    	array(
			'default' => $sliderbtnbgcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'slider_btnbgcolor',
		array(
		    'label'   		=> __('Button BG Color','royal-photography'),
		    'section'		=> 'slider_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	

	

	/*=========================================
	feature Section
	=========================================*/
	$wp_customize->add_section(
		'feature_setting', array(
			'title' => esc_html__( 'Feature Section', 'royal-photography' ),
			'priority' => 2,
			'panel' => 'royalphotography_frontpage_sections',
		)
	);



	$wp_customize->add_setting('royalphotography_feature_tabs', array(
	   'sanitize_callback' => 'wp_kses_post',
	));

	$wp_customize->add_control(new royalphotography_Tab_Control($wp_customize, 'royalphotography_feature_tabs', array(
	   'section' => 'feature_setting',
	   'priority' => 2,
	   'buttons' => array(
	      array(
         	'name' => esc_html__('General', 'royal-photography'),
            'icon' => 'dashicons dashicons-welcome-write-blog',
            'fields' => array(
            	'feature_disable_section',
            	'feature_heading',
            	'feature1',
            	'feature2',
            	'feature3',
            	'feature4'
            ),
            'active' => true,
         ),
	      array(
            'name' => esc_html__('Style', 'royal-photography'),
        	'icon' => 'dashicons dashicons-art',
            'fields' => array(
            	'feature_headingcolor',
				'feature_titlecolor',
				'feature_titlebgcolor'
            ),
     	)
    	),
	))); 



	// General

	// hide show feature section
	$wp_customize->add_setting(
        'feature_disable_section',
        array(
            'sanitize_callback' => 'wp_kses_post',
        )
    ); 
    $wp_customize->add_control(
        new royalphotography_Toggle_Switch_Custom_Control(
            $wp_customize,
            'feature_disable_section',
            array(
                'settings'      => 'feature_disable_section',
                'section'       => 'feature_setting',
                'label'         => __( 'Disable Section', 'royal-photography' ),
                'on_off_label'  => array(
                    'on' => __( 'Yes', 'royal-photography' ),
                    'off' => __( 'No', 'royal-photography' )
                ),
            )
        )
    );


	

    // feature title
	$wp_customize->add_setting(
    	'feature_heading',
    	array(
			'default' => '',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 1,
		)
	);	

	$wp_customize->add_control( 
		'feature_heading',
		array(
		    'label'   		=> __('Heading','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'text',
			'transport'         => $selective_refresh,
		)  
	);	


	// feature 1
	$wp_customize->add_setting( 
    	'feature1',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 1,
		)
	);	

	$wp_customize->add_control( 
		'feature1',
		array(
		    'label'   		=> __('Feature 1','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);		


	// feature 2
	$wp_customize->add_setting(
    	'feature2',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 2,
		)
	);	

	$wp_customize->add_control( 
		'feature2',
		array(
		    'label'   		=> __('Feature 2','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);	

	

	// feature 3
	$wp_customize->add_setting(
    	'feature3',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'feature3',
		array(
		    'label'   		=> __('Feature 3','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);	

	

	// feature 4
	$wp_customize->add_setting(
    	'feature4',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'feature4',
		array(
		    'label'   		=> __('Feature 4','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);

	

	// style

	// feature headingcolor color
	$featureheadingcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'feature_headingcolor',
    	array(
			'default' => $featureheadingcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'feature_headingcolor',
		array(
		    'label'   		=> __('Heading Color','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// feature titlecolor color
	$featuretitlecolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'feature_titlecolor',
    	array(
			'default' => $featuretitlecolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'feature_titlecolor',
		array(
		    'label'   		=> __('Title Color','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// feature titlebgcolor color
	$featuretitlebgcolor = esc_html__('#FFAF33', 'royal-photography' );
	$wp_customize->add_setting(
    	'feature_titlebgcolor',
    	array(
			'default' => $featuretitlebgcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'feature_titlebgcolor',
		array(
		    'label'   		=> __('Title BG Color','royal-photography'),
		    'section'		=> 'feature_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);




	/*=========================================
	Service Section
	=========================================*/
	$wp_customize->add_section(
		'Service_setting', array(
			'title' => esc_html__( 'Service Section', 'royal-photography' ),
			'priority' => 2,
			'panel' => 'royalphotography_frontpage_sections',
		)
	);



	$wp_customize->add_setting('royalphotography_Service_tabs', array(
	   'sanitize_callback' => 'wp_kses_post',
	));

	$wp_customize->add_control(new royalphotography_Tab_Control($wp_customize, 'royalphotography_Service_tabs', array(
	   'section' => 'Service_setting',
	   'priority' => 2,
	   'buttons' => array(
	      array(
         	'name' => esc_html__('General', 'royal-photography'),
            'icon' => 'dashicons dashicons-welcome-write-blog',
            'fields' => array(
            	'service_disable_section',
            	'service_heading',
            	'Service1',
            	'Service2',
            	'Service3',
            	'Service4',
            	'Service5',
            	'Service6'
            ),
            'active' => true,
         ),
	      array(
            'name' => esc_html__('Style', 'royal-photography'),
        	'icon' => 'dashicons dashicons-art',
            'fields' => array(
            	'service_headingcolor',
            	'service_boxtitlecolorcolor',
            	'service_boxdescriptioncolorcolor',
            	'service_buttontextcolor',
				'service_buttonbgcolor'
            ),
     	),
  		array(
	        'name' => esc_html__('Layout', 'royal-photography'),
	        'icon' => 'dashicons dashicons-layout',
	        'fields' => array(
	            'royal_photography_service_section_width',
	            'royalphotography_service_padding',
	            'royal_photography_service_top_padding',
	            'royal_photography_service_bottom_padding'
	        ),
     	)
	    
    	),
	))); 



	// General

	// hide show service section
	$wp_customize->add_setting(
        'service_disable_section',
        array(
            'sanitize_callback' => 'wp_kses_post',
        )
    ); 
    $wp_customize->add_control(
        new royalphotography_Toggle_Switch_Custom_Control(
            $wp_customize,
            'service_disable_section',
            array(
                'settings'      => 'service_disable_section',
                'section'       => 'Service_setting',
                'label'         => __( 'Disable Section', 'royal-photography' ),
                'on_off_label'  => array(
                    'on' => __( 'Yes', 'royal-photography' ),
                    'off' => __( 'No', 'royal-photography' )
                ),
            )
        )
    );


	

    // service title
	$wp_customize->add_setting(
    	'service_heading',
    	array(
			'default' => '',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 1,
		)
	);	

	$wp_customize->add_control( 
		'service_heading',
		array(
		    'label'   		=> __('Heading','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'text',
			'transport'         => $selective_refresh,
		)  
	);	


	// Service 1
	$wp_customize->add_setting( 
    	'Service1',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 1,
		)
	);	

	$wp_customize->add_control( 
		'Service1',
		array(
		    'label'   		=> __('Service 1','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);		


	// Service 2
	$wp_customize->add_setting(
    	'Service2',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 2,
		)
	);	

	$wp_customize->add_control( 
		'Service2',
		array(
		    'label'   		=> __('Service 2','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);	

	

	// Service 3
	$wp_customize->add_setting(
    	'Service3',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'Service3',
		array(
		    'label'   		=> __('Service 3','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);	

	

	// Service 4
	$wp_customize->add_setting(
    	'Service4',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'Service4',
		array(
		    'label'   		=> __('Service 4','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);

	

	// Service 5
	$wp_customize->add_setting(
    	'Service5',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 5,
		)
	);	

	$wp_customize->add_control( 
		'Service5',
		array(
		    'label'   		=> __('Service 5','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);

	
	// Service 6
	$wp_customize->add_setting(
    	'Service6',
    	array(
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 5,
		)
	);	

	$wp_customize->add_control( 
		'Service6',
		array(
		    'label'   		=> __('Service 6','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'dropdown-pages',
			'transport'         => $selective_refresh,
		)  
	);

	

	// style

	// service headingcolor color
	$serviceheadingcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'service_headingcolor',
    	array(
			'default' => $serviceheadingcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'service_headingcolor',
		array(
		    'label'   		=> __('Heading Color','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);


	// service boxtitlecolor color
	$serviceboxtitlecolorcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'service_boxtitlecolorcolor',
    	array(
			'default' => $serviceboxtitlecolorcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'service_boxtitlecolorcolor',
		array(
		    'label'   		=> __('Title Color','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// service boxdescriptioncolor color
	$serviceboxdescriptioncolorcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'service_boxdescriptioncolorcolor',
    	array(
			'default' => $serviceboxdescriptioncolorcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'service_boxdescriptioncolorcolor',
		array(
		    'label'   		=> __('Description Color','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);


	// service buttontext color
	$servicebuttontextcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'service_buttontextcolor',
    	array(
			'default' => $servicebuttontextcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'service_buttontextcolor',
		array(
		    'label'   		=> __('Button Text Color','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// service buttonbg color
	$servicebuttonbgcolor = esc_html__('#FFAF33', 'royal-photography' );
	$wp_customize->add_setting(
    	'service_buttonbgcolor',
    	array(
			'default' => $servicebuttonbgcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 4,
		)
	);	

	$wp_customize->add_control( 
		'service_buttonbgcolor',
		array(
		    'label'   		=> __('Button BG Color','royal-photography'),
		    'section'		=> 'Service_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);



	// layout setting
	$wp_customize->add_setting('royal_photography_service_section_width',array(
        'default' => 'Box Width',
        'sanitize_callback' => 'royalphotography_sanitize_choices',
    ));
    $wp_customize->add_control('royal_photography_service_section_width',array(
        'type' => 'select',
        'label' => __('Section Width','royal-photography'),
        'choices' => array (
            'Box Width' => __('Box Width','royal-photography'),
            'Full Width' => __('Full Width','royal-photography')
        ),
        'section' => 'Service_setting',
    ));


    // service section padding 
	$wp_customize->add_setting('royalphotography_service_padding',array(
      'sanitize_callback'   => 'esc_html'
    ));
    $wp_customize->add_control('royalphotography_service_padding',array(
      'label' => __('Section Padding','royal-photography'),
      'section' => 'Service_setting'
    ));

    $wp_customize->add_setting('royal_photography_service_top_padding',array(
        'default' => '5',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('royal_photography_service_top_padding',array(
	    'type' => 'number',
	    'label' => __('Top','royal-photography'),
	    'section' => 'Service_setting',
    ));

 	$wp_customize->add_setting('royal_photography_service_bottom_padding',array(
        'default' => '2',
        'sanitize_callback' => 'sanitize_text_field'
    ));
    $wp_customize->add_control('royal_photography_service_bottom_padding',array(
	    'type' => 'number',
	    'label' => __('Bottom','royal-photography'),
	    'section' => 'Service_setting',
    ));



    /*=========================================
	Blog Section
	=========================================*/
	$wp_customize->add_section(
		'blog_setting', array(
			'title' => esc_html__( 'Blog Section', 'royal-photography' ),
			'priority' => 3,
			'panel' => 'royalphotography_frontpage_sections',
		)
	);



	

	$wp_customize->add_setting('royalphotography_blog_tabs', array(
	   'sanitize_callback' => 'wp_kses_post',
	));

	$wp_customize->add_control(new royalphotography_Tab_Control($wp_customize, 'royalphotography_blog_tabs', array(
	   'section' => 'blog_setting',
	   'priority' => 2,
	   'buttons' => array(
	      array(
         	'name' => esc_html__('General', 'royal-photography'),
            'icon' => 'dashicons dashicons-welcome-write-blog',
            'fields' => array(
				'blog_disable_section',
            	'blog_heading'
            ),
            'active' => true,
         ), 
	      array(
            'name' => esc_html__('Style', 'royal-photography'),
        	'icon' => 'dashicons dashicons-art',
            'fields' => array(
                'blog_headingcolor',
                'blog_titlecolor',
                'blog_descriptioncolor',
                'blog_btntextcolor',
				'blog_btnbgcolor',
                'blog_datetextcolor',
				'blog_datebgcolor'

            ),
     	),
		 array(
	        'name' => esc_html__('Layout', 'royal-photography'),
	        'icon' => 'dashicons dashicons-layout',
	        'fields' => array(
	            'royal_photography_blog_section_width',
				'royalphotography_blog_padding',
	            'royal_photography_blog_top_padding',
	            'royal_photography_blog_bottom_padding'
	        ),
     	)
	    
    	),
	))); 


	// General Tab

	// hide show blog section
	$wp_customize->add_setting(
        'blog_disable_section',
        array(
            'sanitize_callback' => 'wp_kses_post',
        )
    ); 
    $wp_customize->add_control(
        new royalphotography_Toggle_Switch_Custom_Control(
            $wp_customize,
            'blog_disable_section',
            array(
                'settings'      => 'blog_disable_section',
                'section'       => 'blog_setting',
                'label'         => __( 'Disable Section', 'royal-photography' ),
                'on_off_label'  => array(
                    'on' => __( 'Yes', 'royal-photography' ),
                    'off' => __( 'No', 'royal-photography' )
                ),
            )
        )
    );

	// blog subheading Color
	$wp_customize->add_setting(
    	'blog_heading',
    	array(
			'default' => '',
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 1,
		)
	);	

	$wp_customize->add_control( 
		'blog_heading',
		array(
		    'label'   		=> __('Heading','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'text',
			'transport'         => $selective_refresh,
		)  
	);	


	// Style setting

	// blog heading Color
	$blogheadingcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_headingcolor',
    	array(
			'default' => $blogheadingcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_headingcolor',
		array(
		    'label'   		=> __('Heading Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);


	// blog title Color
	$blogtitlecolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_titlecolor',
    	array(
			'default' => $blogtitlecolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_titlecolor',
		array(
		    'label'   		=> __('Title Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// blog description Color
	$blogdescriptioncolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_descriptioncolor',
    	array(
			'default' => $blogdescriptioncolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_descriptioncolor',
		array(
		    'label'   		=> __('Description Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);


	// blog btntext Color
	$blogbtntextcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_btntextcolor',
    	array(
			'default' => $blogbtntextcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_btntextcolor',
		array(
		    'label'   		=> __('Button Text Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// blog btnbg Color
	$blogbtnbgcolor = esc_html__('#FFAF33', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_btnbgcolor',
    	array(
			'default' => $blogbtnbgcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_btnbgcolor',
		array(
		    'label'   		=> __('Button BG Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// blog datetext Color
	$blogdatetextcolor = esc_html__('#000', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_datetextcolor',
    	array(
			'default' => $blogdatetextcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_datetextcolor',
		array(
		    'label'   		=> __('Date & Comments Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);

	// blog datebg Color
	$blogdatebgcolor = esc_html__('#ffaf33db', 'royal-photography' );
	$wp_customize->add_setting(
    	'blog_datebgcolor',
    	array(
			'default' => $blogdatebgcolor,
			'capability'     	=> 'edit_theme_options',
			'sanitize_callback' => 'wp_kses_post',
			'priority'      => 3,
		)
	);	

	$wp_customize->add_control( 
		'blog_datebgcolor',
		array(
		    'label'   		=> __('Date & Comments BG Color','royal-photography'),
		    'section'		=> 'blog_setting',
			'type' 			=> 'color',
			'transport'         => $selective_refresh,
		)  
	);


	// layout setting
	$wp_customize->add_setting('royal_photography_blog_section_width',array(
        'default' => 'Box Width',
        'sanitize_callback' => 'royalphotography_sanitize_choices',
    ));
    $wp_customize->add_control('royal_photography_blog_section_width',array(
        'type' => 'select',
        'label' => __('Section Width','royal-photography'),
        'choices' => array (
            'Box Width' => __('Box Width','royal-photography'),
            'Full Width' => __('Full Width','royal-photography')
        ),
        'section' => 'blog_setting',
    ));

	// blog section padding 
	$wp_customize->add_setting('royalphotography_blog_padding',array(
		'sanitize_callback'   => 'esc_html'
	));
	$wp_customize->add_control('royalphotography_blog_padding',array(
		'label' => __('Section Padding','royal-photography'),
		'section' => 'blog_setting'
	));

	$wp_customize->add_setting('royal_photography_blog_top_padding',array(
		'default' => '5',
		'sanitize_callback' => 'sanitize_text_field'
	));
	$wp_customize->add_control('royal_photography_blog_top_padding',array(
		'type' => 'number',
		'label' => __('Top','royal-photography'),
		'section' => 'blog_setting',
	));

	$wp_customize->add_setting('royal_photography_blog_bottom_padding',array(
		'default' => '2',
		'sanitize_callback' => 'sanitize_text_field'
	));
	$wp_customize->add_control('royal_photography_blog_bottom_padding',array(
		'type' => 'number',
		'label' => __('Bottom','royal-photography'),
		'section' => 'blog_setting',
	));


	$wp_customize->register_control_type('royalphotography_Tab_Control');

}

add_action( 'customize_register', 'royalphotography_blog_setting' );

// service selective refresh
function royalphotography_blog_section_partials( $wp_customize ){	
	// blog_title
	$wp_customize->selective_refresh->add_partial( 'blog_title', array(
		'selector'            => '.home-blog .title h6',
		'settings'            => 'blog_title',
		'render_callback'  => 'royalphotography_blog_title_render_callback',
	
	) );
	
	// blog_subtitle
	$wp_customize->selective_refresh->add_partial( 'blog_subtitle', array(
		'selector'            => '.home-blog .title h2',
		'settings'            => 'blog_subtitle',
		'render_callback'  => 'royalphotography_blog_subtitle_render_callback',
	
	) );
	
	// blog_description
	$wp_customize->selective_refresh->add_partial( 'blog_description', array(
		'selector'            => '.home-blog .title p',
		'settings'            => 'blog_description',
		'render_callback'  => 'royalphotography_blog_description_render_callback',
	
	) );	
	}

add_action( 'customize_register', 'royalphotography_blog_section_partials' );

// blog_title
function royalphotography_blog_title_render_callback() {
	return get_theme_mod( 'blog_title' );
}

// blog_subtitle
function royalphotography_blog_subtitle_render_callback() {
	return get_theme_mod( 'blog_subtitle' );
}

// service description
function royalphotography_blog_description_render_callback() {
	return get_theme_mod( 'blog_description' );
}


