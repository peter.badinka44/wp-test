( function( api ) {

	// Extends our custom "royal-photography" section.
	api.sectionConstructor['royal-photography'] = api.Section.extend( {

		// No events for this type of section.
		attachEvents: function () {},

		// Always make the section active.
		isContextuallyActive: function () {
			return true;
		}
	} );

} )( wp.customize );