=== Royal Photography ===

Contributors: Peccular
Requires at least: 4.4
Tested up to: 5.8
Requires PHP: 5.6
Stable tag: 0.1.4
Version: 0.1.4
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, left-sidebar, right-sidebar, flexible-header, custom-background, custom-colors, custom-header, custom-menu, featured-image-header, featured-images, footer-widgets, full-width-template, theme-options, threaded-comments, translation-ready, blog, grid-layout, portfolio, photography

== Copyright ==

royal-photography WordPress Theme, Copyright 2021 Peccular
royal-photography is distributed under the terms of the GNU GPL


== Description ==

Royal Photography WordPress Theme is perfect for personal, lifestyle, food, travel, fashion, or corporate photography.  Take a closer look and you will love it! Very easy to use and manage even for those who’re just starting their online photography journey and know very little about WordPress. Royal Photography WordPress Theme we’ve designed is clean & modern design that will showcase your business on any device without slightest distortion. Just place images and content of your choice and you’re ready to go! Neither requires investment, not extra heavy plug-ins; it is quick to load, and looks great!
People using Royal Photography WordPress Theme designed by us knew very little about WordPress theme in the beginning; it took them a few days to get familiar with it, but once they managed to get a swing of it they enjoyed great returns.  The theme gives online attention & conversions, both. People using it enjoy the freedom and flexibility of Royal Photography WordPress Theme.  They can change images, edit and update with newsworthy content, change background colors, add logos, or whatever they want to do to make it look chic. You can also go for it and enjoy the benefits too. 


== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in royal-photography WordPress Theme
-------------------------------------------------------------------------------------------------

1) Package Structure
Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

2) Font Awesome
=================================================================================================
Font Awesome 4.6.3 by @davegandy - http://fontawesome.io - @fontawesome
License - License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
Source: http://fontawesome.io

3) Bootstrap Framework
=================================================================================================
Bootstrap v5.1.0 (https://getbootstrap.com/)
Copyright 2011-2021 The Bootstrap Authors
Copyright 2011-2021 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)

4) Owl Carousel 2.3.4
=================================================================================================
Owl Carousel 2.3.4 - by @David Deutsch - https://github.com/OwlCarousel2
License - MIT License(https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE)

5) WOW Js
=================================================================================================
WOW (https://wowjs.uk/)
Copyright (c) 2016 Thomas Grainger.
License - MIT License (https://github.com/graingert/WOW/blob/master/LICENSE)

6) Animate Css
Animate Css by @Daniel Eden - http://daneden.me/animate
License - MIT License(https://github.com/daneden/animate.css/blob/master/LICENSE) 
Source: https://github.com/daneden/animate.css

7) Sainitization
=================================================================================================
License - GNU General Public License (https://github.com/WPTRT/code-examples/blob/master/LICENSE)
Source: https://github.com/WPTRT/code-examples

8) wp-bootstrap-navwalker
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt)
Source: https://github.com/wp-bootstrap/wp-bootstrap-navwalker


9) Swiper
=================================================================================================
Swiper 4.0.7
Most modern mobile touch slider and framework with hardware accelerated transitions
http://www.idangero.us/swiper/
Copyright 2014-2017 Vladimir Kharlampidi
Released under the MIT License
Released on: November 28, 2017


10) Screenshot Image
=================================================================================================
Slider image
URL: https://pxhere.com/en/photo/990040
Source: https://pxhere.com/
License: Creative Commons Zero

Feature image
URL: https://pxhere.com/en/photo/14200
Source: https://pxhere.com/
License: Creative Commons Zero

Feature image
URL: https://pxhere.com/en/photo/210699
Source: https://pxhere.com/
License: Creative Commons Zero

Feature image
URL: https://pxhere.com/en/photo/612446
Source: https://pxhere.com/
License: Creative Commons Zero

Feature image
URL: https://pxhere.com/en/photo/1280716
Source: https://pxhere.com/
License: Creative Commons Zero


11) Image Folder Images
Clip Art & All other Images have been used in images folder, Created by Peccular. Also they are GPL Licensed and free to use and free to redistribute further.

== Changelog ==

@version 0.1
* Initial release

@version 0.1.1
* Mobile Menu Fixed.

@version 0.1.2
* Added Checkbox Option in Header For Display Site Title and Tagline.
* Added BG Color Option in Header.

@version 0.1.3
* Feature Disable Section Setting Error Fixed.
* Added Toggle for Disable Section in Blog Customizer.
* Added Layout Tab in Blog Customizer.
* Added Section Width Setting in Blog Customizer.

@version 0.1.4
* Added Section Padding Setting in Blog Section Customizer.
* Added Links to Services Image & Title.