<section id="feature-section" class="features-area home-features">

		<div class="row justify-content-center text-center"> 
	
			<div class="header-section">
				<h3 class="section-title">
					<div class="himg">
                        <?php 
                            $h_image = get_theme_mod('h_image');
                            if(!empty($h_image)){
                              echo '<img alt="'. esc_html(get_the_title()) .'" src="'.esc_url($h_image).'" class="img-responsive secondry-bg-img" />';
                            }else{
                              echo '<img src="'.get_template_directory_uri().'/assets/images/hcamera.png" class="img-responsive" />';
                            }
                        ?>
                    </div>
					<?php echo esc_html(get_theme_mod('feature_heading')); ?>
					
				</h3>
				<div class="hbimg">
                    <?php 
                        $hb_image = get_theme_mod('hb_image');
                        if(!empty($hb_image)){
                          echo '<img alt="'. esc_html(get_the_title()) .'" src="'.esc_url($hb_image).'" class="img-responsive secondry-bg-img" />';
                        }else{
                          echo '<img src="'.get_template_directory_uri().'/assets/images/hbshadow.png" class="img-responsive" />';
                        }
                    ?>
                </div>
					
				<div class="clearfix"></div>
			</div>
		</div> 
		<div class="mfbox">
			<div class="f-tp"></div>
			<div class="mfboxinn">
				<div class="container">
					<div class="row mr-0">

						<?php for($p=1; $p<7; $p++) { ?>
				        <?php if( get_theme_mod('feature'.$p,false)) { ?>
				        <?php $querycolumns = new WP_query('page_id='.get_theme_mod('feature'.$p,true)); ?>
				        <?php while( $querycolumns->have_posts() ) : $querycolumns->the_post(); 
				          $image = wp_get_attachment_image_src(get_post_thumbnail_id() , true); ?>
				        <?php 
				          if(has_post_thumbnail()){
				            $img = esc_url($image[0]);
				          }
				          if(empty($image)){
				            $img = get_template_directory_uri().'/assets/images/default.png';
				          }
				        ?>
						
						<!-- Start Single feature -->
						<div class="col-md-6 col-lg-3 box-space text-center">
							<div class="threebox box<?php echo esc_attr( $p ) ?> <?php if($p % 3 == 0) { echo "last_column"; } ?>">    
								<div class="single-feature">
									<div class="imageBox">
					                	<img src="<?php echo esc_url($img); ?>" alt="<?php the_title_attribute(); ?>">
					                	<div class="conbx">
											<h3 class="title"><?php the_title_attribute(); ?></h3>
										</div>
					                </div> 
									
									<div class="clearfix"></div>
								</div>
			              	</div>
						</div>
						<!-- / End Single feature -->

						<?php endwhile;
			           wp_reset_postdata(); ?>
			        <?php } } ?>
			        <div class="clear"></div> 
						
					</div>
				</div>
			</div>
			<div class="f-bp"></div>
		</div>
</section>
