<section id="slider-section" class="slider-area home-slider">
  <!-- start of hero --> 
    <section class="hero-slider hero-style">
        <div class="royal_photographyswiper-container">
            <div class="swiper-wrapper">
              <?php for($p=1; $p<6; $p++) { ?>
              <?php if( get_theme_mod('slider'.$p,false)) { ?>
              <?php $querycolumns = new WP_query('page_id='.get_theme_mod('slider'.$p,true)); ?>
              <?php while( $querycolumns->have_posts() ) : $querycolumns->the_post(); 
                $image = wp_get_attachment_image_src(get_post_thumbnail_id() , true); ?>

              <?php 
                if(has_post_thumbnail()){
                  $img = esc_url($image[0]);
                }
                if(empty($image)){
                  $img = get_template_directory_uri().'/assets/images/default.png';
                }

              ?>
                <div class="royal_photographyswiper-slide">
                    <div class="royal_photographyslide-inner slide-bg-image">
                        <div class="row md-0">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="sliderimg">
                                    <div class="s-img-brd"></div>
                                      <div class="sliderimg-inn">
                                        
                                        <img class="slide-mainimg slidershape1" src="<?php echo esc_url($img); ?>" alt="<?php the_title_attribute(); ?>">
                                      </div>
                                      <div class="clearfix"></div>
                                </div>
                                <div class="gbrd1"></div>
                                <div class="gbrd2"></div>
                                <div class="gbox"></div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="slidersvg2">              
                                    <div class="slidercontent">
                                        <div class="contentbx">
                                            <div class="ctimg">
                                                <?php 
                                                  $ct_image = get_theme_mod('ct_image');
                                                  if(!empty($ct_image)){
                                                    echo '<img alt="'. esc_html(get_the_title()) .'" src="'.esc_url($ct_image).'" class="img-responsive secondry-bg-img" />';
                                                  }else{
                                                    echo '<img src="'.get_template_directory_uri().'/assets/images/slide-con-t.png" class="img-responsive" />';
                                                  }
                                                ?>
                                            </div>
                                            <div class="slide-title">
                                                <h2><?php the_title_attribute(); ?></h2>   
                                            </div>    
                                            <div class="slide-text" >
                                                <?php the_excerpt(); ?>
                                            </div>
                                            <div class="slide-btns">

                                                <a class="ReadMore" href="<?php echo esc_url( get_permalink() ); ?>"><?php esc_html_e('READ MORE','royal-photography'); ?></a>
                                            </div>
                                            <div class="cbimg">
                                                  <?php 
                                                    $cb_image = get_theme_mod('cb_image');
                                                    if(!empty($cb_image)){
                                                      echo '<img alt="'. esc_html(get_the_title()) .'" src="'.esc_url($cb_image).'" class="img-responsive secondry-bg-img" />';
                                                    }else{
                                                      echo '<img src="'.get_template_directory_uri().'/assets/images/slide-con-b.png" class="img-responsive" />';
                                                    }
                                                  ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              <?php endwhile;
                 wp_reset_postdata(); ?>
              <?php } } ?>
              <div class="clear"></div> 

            </div>
           <!-- swipper controls -->
              <div class="royal_photographyswiper-pagination"></div>
              <div class="royal_photographyswiper-button-next"><i class="fa fa-angle-right"></i></div>
              <div class="royal_photographyswiper-button-prev"><i class="fa fa-angle-left"></i></div>
        </div>

        <div class="bt-img">
            <?php 
              $cb_image = get_theme_mod('cb_image');
              if(!empty($cb_image)){
                echo '<img alt="'. esc_html(get_the_title()) .'" src="'.esc_url($cb_image).'" class="img-responsive secondry-bg-img" />';
              }else{
                echo '<img src="'.get_template_directory_uri().'/assets/images/slider-b-text.png" class="img-responsive" />';
              }
            ?>

          
        </div>
    </section>
  <!-- end of hero slider -->
</section>
