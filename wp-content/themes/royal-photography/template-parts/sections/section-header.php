<?php if ( get_header_image() ) : ?>
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="custom-header" rel="home">
		<img src="<?php esc_url(header_image()); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo esc_attr(get_bloginfo( 'title' )); ?>">
	</a>	
<?php endif;  ?>
<!-- Header Area -->

	<?php 
		// header 
		$stickyheader = esc_attr(royalphotography_sticky_menu());
	?>
<div class="main">
    <header class="main-header site-header <?php echo esc_attr(royalphotography_sticky_menu()); ?>">
    	<div class="tp-h"></div>
		<div class="container-fluid">
			<div class="header-section">
				<div class="headfer-content row m-rl">
					<div class="col-lg-3 col-md-4 col-sm-6 col-12 logo-box">
						<div class="site-logo">
							<?php
							if(has_custom_logo())
								{	
									the_custom_logo();
								}
								else { 
							?>
							<a class="site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>">	
								<?php 
									echo esc_html(bloginfo('name'));
								?>
							</a>	
							<?php 						
								}
							?>
						</div>
						<div class="box-info">
							<?php
								$royalphotography_site_desc = get_bloginfo( 'description');
								if ($royalphotography_site_desc) : ?>
									<p class="site-description"><?php echo esc_html($royalphotography_site_desc); ?></p>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-6 col-12">
						<div class="row">
							<div class="col-lg-9 col-md-2 col-sm-1">
								<div class="menu">
									<button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
										<span class="toggle-inner">
											<span class="toggle-icon">
												<i class="fa fa-bars"></i>
											</span>
										</span>
									</button><!-- .nav-toggle -->
									<div class="header-navigation-wrapper">

									<?php
									if ( has_nav_menu( 'primary_menu' ) || ! has_nav_menu( 'expanded' ) ) {
									?>

										<nav class="primary-menu-wrapper" aria-label="<?php echo esc_attr_x( 'Horizontal', 'menu', 'royal-photography' ); ?>">

											<ul class="primary-menu reset-list-style">
												

											<?php
											if ( has_nav_menu( 'primary_menu' ) ) {

												wp_nav_menu(
													array(
														'container'  => '',
														'items_wrap' => '%3$s',
														'theme_location' => 'primary_menu',
													)
												);

											} 
											
											elseif ( ! has_nav_menu( 'expanded' ) ) {

												wp_list_pages(
													array(
														'match_menu_classes' => true,
														'show_sub_menu_icons' => true,
														'title_li' => false,
														'walker'   => new RoyalPhotography_Walker_Page(),
													)
												);

											}

											?>
										
											</ul>

										</nav><!-- .primary-menu-wrapper -->

									<?php } ?>
									</div><!-- .header-navigation-wrapper -->
									<?php
									// Output the menu modal.
									get_template_part( 'template-parts/sections/modal-menu' );
									?>

								</div>
								
							</div>
							<div class="col-lg-3 col-md-10 col-sm-10 ">
								<div class="mb-n">
									<div class="hbtn">
										<div class="contact">
											<div class="contact-btn">
											<a class="btn" href="tel:<?php echo esc_html(get_theme_mod('topheader_btntext')); ?>">
												<i class="fa fa-phone"></i><?php echo esc_html(get_theme_mod('topheader_btntext')); ?></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="tp-b"></div>
    </header>
	<div class="clearfix"></div>
</div>

