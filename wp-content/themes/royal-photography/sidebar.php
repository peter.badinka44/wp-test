<?php if ( ! is_active_sidebar( 'royal-photography-sidebar-primary' ) ) {	return; } ?>
<div class="col-lg-4">
	<div class="sidebar">
		<?php dynamic_sidebar('royal-photography-sidebar-primary'); ?>
	</div>
</div>